﻿using Framework.utils;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Interfaces;

namespace Framework.pages
{
    public class Email
    {
        private readonly IWebDriver driver;

        public Email(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void CopyEmailAddress()
        {
            driver.Navigate().GoToUrl(ConfigurationManager.Instance.EmailServiceUrl);
            Thread.Sleep(TimeSpan.FromSeconds(7));
            Consent?.Click();
            EstimateCostValue.EmailAddress = EmailInput?.GetAttribute("value");
        }



        public void ReadEmailAddress()
        {
            OpenMail?.Click();
            Thread.Sleep(2000);
            driver.SwitchTo().Frame(Frame);
            Thread.Sleep(2000);
            EstimateCostValue.EstimatedValueEmail = MailBody?.Text;
        }

        [FindsBy(How = How.CssSelector, Using = "li.mail-item")]
        public IWebElement? OpenMail { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#fullmessage")]
        public IWebElement? Frame { get; set; }

        [FindsBy(How = How.XPath, Using = "//tr[2]/td[2]/h3")]
        public IWebElement? MailBody { get; set; }

        [FindsBy(How = How.CssSelector, Using = "button.fc-button.fc-cta-consent.fc-primary-button")]
        public IWebElement? Consent { get; set; }

        [FindsBy(How = How.Id, Using = "i-email")]
        public IWebElement? EmailInput { get; set; }


        
    }
}
