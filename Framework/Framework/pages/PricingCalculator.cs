﻿using Framework.utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System.Text;

namespace Framework.pages
{
    public class PricingCalculator
    {
        private readonly IWebDriver driver;

        public PricingCalculator(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void FillTheFields(string instancesNumber)
        {
           
            CloseHelpWindow();

 
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("#cloud-site > devsite-iframe > iframe")));
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("#myFrame")));

            Instances?.SendKeys(instancesNumber);


            Series?.Click();
            SeriesSelect?.Click();

            MachineType?.Click();


            MachineTypeSelect?.Click();
            AddGPUs?.Click();
            Thread.Sleep(2000);
            GpuType?.Click();
            Thread.Sleep(1500);
            GpuTypeSelect?.Click();

            NumberSelect?.Click();
            Thread.Sleep(1500);
            NumberOfGPUSelect?.Click();
            
            LocalSSD?.Click();
            Thread.Sleep(1500);
            LocalSSDSelect?.Click();

            DataCenterLocation?.Click();
            Thread.Sleep(1500);
            DataCenterLocationSelect?.Click();

            CommittedUsage?.Click();
            Thread.Sleep(1500);
            CommittedUsageSelect?.Click();

            Thread.Sleep(1500);
            AddToEstimate?.Click();
            EstimateCostValue.EstimatedValueGoogleCloud = EstimateCost?.Text;

        }


        public void GetCost()
        {
            string? estimatedValue = EstimateCostValue.EstimatedValueGoogleCloud;

            StringBuilder price = new StringBuilder();
            for (int i = 26; i < 38; i++)
            {
                price.Append(estimatedValue![i]);
            }

            EstimateCostValue.EstimatedValueGoogleCloud = price.ToString();
        }



        public void SendPriceToEmail(string? email)
        {
            EmailEstimate?.Click();
            EmailValue?.SendKeys(email);
            SendEmailBtn?.Click();
        }

        [FindsBy(How = How.CssSelector, Using = "button.md-raised.md-primary.cpc-button[type=\"button\"][ng-click=\"emailQuote.emailQuote(true); emailQuote.$mdDialog.hide()\"]")]
        public IWebElement? SendEmailBtn { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#input_615")]
        public IWebElement? EmailValue { get; set; }

        [FindsBy(How = How.XPath, Using = "//button[@id='Email Estimate']")]
        public IWebElement? EmailEstimate { get; set; }
        
        
        [FindsBy(How = How.CssSelector, Using = "cloudx-chat[bubble-text=\"Hi there \ud83d\udc4b Have questions about our pricing?\"")]
        public IWebElement? ChatElement { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#cloud-site > devsite-iframe > iframe")]
        public IWebElement? IFrameElement { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#myFrame")]
        public IWebElement? IFrameElement1 { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#input_99")]
        public IWebElement? Instances { get; set; }

        [FindsBy(How = How.XPath, Using = "//md-select-value[@id='select_value_label_94']//span[@class='md-select-icon']")]
        public IWebElement? Series { get; set; }

        [FindsBy(How = How.XPath, Using = "//md-option[@id='select_option_221']")]
        public IWebElement? SeriesSelect { get; set; }

        [FindsBy(How = How.XPath, Using = "//md-select-value[@id='select_value_label_95']//span[@class='md-select-icon']")]
        public IWebElement? MachineType { get; set; }

        [FindsBy(How = How.Id, Using = "select_option_469")]
        public IWebElement? MachineTypeSelect { get; set; }

        [FindsBy(How = How.CssSelector, Using = "md-checkbox[aria-label='Add GPUs']")]
        public IWebElement? AddGPUs { get; set; }
        [FindsBy(How = How.CssSelector, Using = "[ng-model='listingCtrl.computeServer.gpuType']")]
        public IWebElement? GpuType { get; set; }

        [FindsBy(How = How.XPath, Using = "//md-option[@id='select_option_513']")]
        public IWebElement? GpuTypeSelect { get; set; }
        [FindsBy(How = How.XPath, Using = "//md-select[@id='select_508']")]
        public IWebElement? NumberSelect { get; set; }

        [FindsBy(How = How.XPath, Using = "//md-option[@id='select_option_516']")]
        public IWebElement? NumberOfGPUSelect { get; set; }

        [FindsBy(How = How.XPath, Using = "//md-select-value[@id='select_value_label_464']//span[@class='md-select-icon']")]
        public IWebElement? LocalSSD { get; set; }

        [FindsBy(How = How.CssSelector, Using = "#select_option_490")]
        public IWebElement? LocalSSDSelect { get; set; }

        [FindsBy(How = How.XPath, Using = "//md-select-value[@id='select_value_label_97']//span[@class='md-select-icon']")]
        public IWebElement? DataCenterLocation { get; set; }

        [FindsBy(How = How.XPath, Using = "//md-option[@id='select_option_264']")]
        public IWebElement? DataCenterLocationSelect { get; set; }

        //

        [FindsBy(How = How.XPath, Using = "//md-select-value[@id='select_value_label_98']//span[@class='md-select-icon']")]
        public IWebElement? CommittedUsage { get; set; }

        [FindsBy(How = How.XPath, Using = "//md-option[@id='select_option_137']")]
        public IWebElement? CommittedUsageSelect { get; set; }

        [FindsBy(How = How.CssSelector, Using = "form[name='ComputeEngineForm'] div[class='layout-align-end-start layout-row'] button[type='button']")]
        public IWebElement? AddToEstimate { get; set; }

        [FindsBy(How = How.CssSelector, Using = "b.ng-binding")]
        public IWebElement? EstimateCost { get; set; }

        private void CloseHelpWindow()
        {
            IJavaScriptExecutor javaScriptExecutor = (IJavaScriptExecutor)driver;
            IWebElement closeSpan = (IWebElement)javaScriptExecutor.ExecuteScript("return arguments[0].shadowRoot.querySelector('.close');", ChatElement);
            closeSpan.Click();
        }


        

    }
}
