﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System.Text;

namespace Framework.pages
{
    
    public class GoogleCloudMainPage
    {
        public void OpenGoogleCalculator(string searchTerm)
        {
            OkBtn?.Click();
            SearchBtn?.Click();
            SearchItem(searchTerm);
            Calculator?.Click();
        }

        
        [FindsBy(How = How.ClassName, Using = "pvUife")]
        public IWebElement? OkBtn { get; set; }

        [FindsBy(How = How.ClassName, Using = "YSM5S")]
        public IWebElement? SearchBtn { get; set; }

        [FindsBy(How = How.CssSelector, Using = "input[placeholder='Search']")]
        public IWebElement? InputSearch { get; set; }


        [FindsBy(How = How.CssSelector, Using = "div.gsc-thumbnail-inside div.gs-title a.gs-title")]
        public IWebElement? Calculator { get; set; }


        private void SearchItem(string searchTerm)
        {
            InputSearch?.SendKeys(searchTerm);
            InputSearch?.SendKeys(Keys.Enter);
        }

    }
}
