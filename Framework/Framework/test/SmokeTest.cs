using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using Framework.pages;
using Framework.driver;
using Framework.utils;
using NUnit.Framework;
using SeleniumExtras.PageObjects;

namespace Framework
{
    public class SmokeTest : CommonConditions
    {

        [Test]
        [Category("Smoke")]
        public void CloudGoogleEstimatedSum()
        {
            
            var mainPage = new GoogleCloudMainPage();
            PageFactory.InitElements(driver, mainPage);
            mainPage.OpenGoogleCalculator("Google Cloud Platform Pricing Calculator");

            var pricingCalculatorPage = new PricingCalculator(driver);
            PageFactory.InitElements(driver, pricingCalculatorPage);
            pricingCalculatorPage.FillTheFields("4");

            EstimateCostValue pageSwitcher = new EstimateCostValue(driver);

            pageSwitcher.SwitchToNewTab();

            var tempEmailPage = new Email(driver);
            PageFactory.InitElements(driver, tempEmailPage);
            tempEmailPage.CopyEmailAddress();

            driver.SwitchTo().Window(EstimateCostValue.FirstTab);

            driver.SwitchTo().Frame(pricingCalculatorPage.IFrameElement);
            driver.SwitchTo().Frame(pricingCalculatorPage.IFrameElement1);

            pricingCalculatorPage.GetCost();

            pricingCalculatorPage.SendPriceToEmail(EstimateCostValue.EmailAddress);
            
            pageSwitcher.SwitchToEmailPage();

            tempEmailPage.ReadEmailAddress();

            Assert.IsTrue(EstimateCostValue.EstimatedValueEmail == EstimateCostValue.EstimatedValueGoogleCloud);

        }

    }
}