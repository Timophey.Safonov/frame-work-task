﻿using Framework.utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;

namespace Framework.driver
{
    public class DriverInstance
    {
        private static IWebDriver? driver;

        private DriverInstance() {}


        public static IWebDriver GetInstance(string? browserName)
        {

            if (driver == null)
            {
                switch (browserName?.ToLower())
                {
                    case "chrome":
                    {
                        new DriverManager().SetUpDriver(new ChromeConfig());
                        driver = new ChromeDriver();
                        break;
                    }

                    case "edge":
                    {
                        new DriverManager().SetUpDriver(new EdgeConfig());
                        driver = new EdgeDriver();
                        break;
                    }

                    default:
                        new DriverManager().SetUpDriver(new ChromeConfig());
                        driver = new ChromeDriver();
                        break;
                }
                driver.Url = ConfigurationManager.Instance.BaseUrl;
                driver.Manage().Window.Maximize();
            }

            return driver;
        }

        public static void CloseBrowser()
        {
            driver?.Quit();
            driver = null;
        }
    }
}
