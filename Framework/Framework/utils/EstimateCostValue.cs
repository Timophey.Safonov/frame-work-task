﻿using OpenQA.Selenium;


namespace Framework.utils
{
    public class EstimateCostValue
    {

        private IWebDriver driver;

        public EstimateCostValue(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void SwitchToEmailPage()
        {
            driver.SwitchTo().Window(driver.WindowHandles[1]);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            long windowHeight = (long)js.ExecuteScript("return window.innerHeight;");
            long scrollAmount = windowHeight / 2;
            js.ExecuteScript($"window.scrollBy(0, {scrollAmount});");
            Thread.Sleep(TimeSpan.FromSeconds(45));
        }

        public void SwitchToNewTab()
        {
            FirstTab = driver.CurrentWindowHandle;
            IJavaScriptExecutor javaScriptExecutor = (IJavaScriptExecutor)driver;
            javaScriptExecutor.ExecuteScript("window.open()");
            driver.SwitchTo().Window(driver.WindowHandles[1]);
        }


        public static string? EstimatedValueGoogleCloud { get; set; }
        public static string? EstimatedValueEmail { get; set; }

        public static string? FirstTab { get; set; }

        public static string? EmailAddress { get; set;}


        

    }
}
