﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.driver;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using OpenQA.Selenium;

namespace Framework.utils
{
    [TestFixture]
    [Category("CommonConditions")]
    [Parallelizable(ParallelScope.Fixtures)]
    public class CommonConditions
    {
        protected IWebDriver driver;

        private TestListener testListner;


        [SetUp]
        public void SetUp()
        {
            driver = DriverInstance.GetInstance(ConfigurationManager.Instance.WebBrowser);
            testListner = new TestListener(driver);
        }

        [TearDown]
        public void TearDown()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                testListner.CaptureScreenshot(TestContext.CurrentContext.Test.FullName);
                
            }
            DriverInstance.CloseBrowser();
        }

    }
}
