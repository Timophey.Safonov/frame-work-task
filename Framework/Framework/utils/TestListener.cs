﻿using NUnit.Framework.Interfaces;
using OpenQA.Selenium;

namespace Framework.utils
{
    public class TestListener : ITestListener
    {
        private readonly IWebDriver driver;

        public TestListener(IWebDriver driver)
        {
            this.driver = driver;
        }


        public void TestStarted(ITest test)
        {

        }

        public void TestFinished(ITestResult result)
        {

        }

        public void TestOutput(TestOutput output)
        {

        }

        public void SendMessage(TestMessage message)
        {

        }

        public void CaptureScreenshot(string testName)
        {
            ITakesScreenshot screenshotDriver = (ITakesScreenshot)driver;
            Screenshot screenshot = screenshotDriver.GetScreenshot();
            string timestamp = DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss_fff");
            string screenshotFileName = $"{testName}_{timestamp}_FailureScreenshot.png";
            string artifactsDirectory = "artifacts";
            string screenshotPath = Path.Combine(artifactsDirectory, screenshotFileName);

            if (!Directory.Exists(artifactsDirectory))
            {
                Directory.CreateDirectory(artifactsDirectory); 
            }

            screenshot.SaveAsFile(screenshotPath, ScreenshotImageFormat.Png);

        }
    }
}
