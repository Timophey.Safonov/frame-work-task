﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System;
using System.IO;


namespace Framework.utils
{
    public class ConfigurationManager
    {
        private static readonly Lazy<ConfigurationManager> instance = new(() => new ConfigurationManager());

        public static ConfigurationManager Instance => instance.Value;

        private readonly EnvironmentConfiguration? currenEnvironment;

        private ConfigurationManager()
        {
            string environment = Environment.GetEnvironmentVariable("TEST_ENVIRONMENT") ?? "dev";
            string configFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "resources", $"config.{environment}.json");

            if (!File.Exists(configFilePath))
            {
                throw new FileNotFoundException($"Configuration file '{configFilePath}' not found.");
            }

            string configJson = File.ReadAllText(configFilePath);
            currenEnvironment = JsonConvert.DeserializeObject<EnvironmentConfiguration>(configJson);
        }

        public string? BaseUrl => currenEnvironment?.BaseUrl;

        public string? WebBrowser => currenEnvironment?.WebBrowser;

        public string? EmailServiceUrl => currenEnvironment?.EmailServiceUrl;

        private class EnvironmentConfiguration
        {
            public string? WebBrowser { get; set; }
            public string? BaseUrl { get; set; }

            public string? EmailServiceUrl { get; set; }
        }

    }
}